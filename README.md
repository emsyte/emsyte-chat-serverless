# Emsyte Chat Serverless

Serverless chat app for Emsyte.

## Getting started
This project uses [serverless](https://serverless.com/) to deploy to AWS. To get started, first you'll need to install all the npm dependencies:

```bash
npm install
```

Than you can install the local DynamoDB instance:

```bash
serverless dynamodb install
```

Next, you need to install the python requirements:

```bash
pipenv install
```

Finally, you can run the app locally:

```bash
pipenv run serverless offline start
```

### Testing
The easiest way to test the app is to use [wscat](https://www.npmjs.com/package/wscat):

```bash
npm install -g wscat

wscat -c ws://localhost:4001

Connected (press CTRL+C to quit)

> {}
< {"message": "Connect successful.", "status_code": 200, "sender": "connection_manager"}
< {"message": "Unrecognized WebSocket action.", "status_code": 400, "sender": "default_message"}
```

### Using with the front-end
To use with the front-end, make sure `REACT_APP_CHAT_WEBSOCKET_URL` is set to the WS URL (by default it's set to `ws://localhost:4001` which should be correct)

## Actions
These are the actions available:

**auth**

Authenticate the current chat session. You can't do anything else until authenticated. To get an authentication token, use this backend endpoint (`chat-auth-token`).

Example:
```json
{"action": "auth", "token": "<token gotten from backend>"}
```

**getRecentMessages**

Get the 10 most recent chat messages.

Example:
```json
{"action": "getRecentMessages"}
```

**sendMessage**

Send a message:

Example:
```json
{"action": "sendMessage", "content": "Hello world!"}
```

## Architecture
The architecture for this app is based on this [article](https://levelup.gitconnected.com/creating-a-chat-app-with-serverless-websockets-and-python-a-tutorial-54cbc432e4f).

First, the user connects to the API gateway via a websocket. When a connection is created, a row gets added to the `ConnectionsTable` to track each session.

To authenticate, the user must first get an authentication token from the backend (using the `reaction/session/<session id>/chat-auth-token` API). This authentication token is a JSON-Web-Token that contains the following information:

```json
{
    "eventId": 47,
    "username": "john.doe",
    "viewer_id": 45
}
```

When a user authenticates, the session information in the `ConnectionsTable` is updated with the token details. When a message is sent, it's added to the `MessagesTable`.
Messages are split out based on their `eventId` (e.g. the `eventId` field signifies the chat channel).
Whenever a message is added to the `MessagesTable`, it's sent to all WebSockets that are authenticated on the same `eventId`.

### Chat Insertion
There is an [SQS](https://aws.amazon.com/sqs/) stream called the `FeedInStream` that is consumed by the `feedInMessage` function.
Whenever a message is sent to the queue, it will be added to the `MessagesTable` and it will in-turn be sent to all websocket connections on the same `eventId`.