import jwt
from .constants import JWT_SIGNING_KEY, JWT_ALGORITHM
from .utils import StatusError
from .models import Session

def authenticate_token(token):
    try:
        data = jwt.decode(token, JWT_SIGNING_KEY, algorithms=[JWT_ALGORITHM], verify=True)
        return Session(
            username=data['username'],
            eventId=str(data['eventId']),
            viewer_id=data['viewer_id']
        )
    except:
        raise StatusError(400, 'Failed to authenticate')
