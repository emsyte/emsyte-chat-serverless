import json
import logging
import time
from dataclasses import dataclass

import boto3
import jwt
from boto3.dynamodb.conditions import Attr

from .auth import authenticate_token
from .constants import (CONNECTIONS_TABLE, IS_OFFLINE, JWT_ALGORITHM,
                        JWT_SIGNING_KEY, MESSAGES_TABLE, REQUEST_ID_FIELD, TTL_FIELD)
from .models import Session
from .utils import (StatusError, add_request_id, get_body, get_connection_id,
                    get_request_id, get_response, handler, send_to_connection, get_expiration)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

if IS_OFFLINE:
    dynamodb = boto3.resource("dynamodb", endpoint_url="http://localhost:5000")
else:
    dynamodb = boto3.resource("dynamodb")




@handler
def connection_manager(event, context):
    """
    Handles connecting and disconnecting for the Websocket.
    """
    connection_id = get_connection_id(event)

    if event["requestContext"]["eventType"] == "CONNECT":
        logger.info("Connect requested")

        # Add connectionID to the database
        table = dynamodb.Table(CONNECTIONS_TABLE)
        table.put_item(Item={"connectionId": connection_id})
        return get_response(200, "Connect successful.")

    elif event["requestContext"]["eventType"] == "DISCONNECT":
        logger.info("Disconnect requested")

        # Remove the connectionID from the database
        table = dynamodb.Table(CONNECTIONS_TABLE)
        table.delete_item(Key={"connectionId": connection_id})
        return get_response(200, "Disconnect successful.")

    else:
        logger.error("Connection manager received unrecognized eventType %s",
                     event["requestContext"]["eventType"])
        return get_response(500, "Unrecognized eventType.")


def get_session(connection_id):
    table = dynamodb.Table(CONNECTIONS_TABLE)
    response = table.get_item(Key={"connectionId": connection_id})

    try:
        return Session(
            eventId=response['Item']['eventId'],
            username=response['Item']['username'],
            registrantId=int(response['Item']['registrantId'])
        )
    except:
        raise StatusError(401, "Unauthorized")


@handler
def auth(event, context):
    # TODO: To authenticate first the client should hit a URL on the backend to get a signed JWT with the necessary info
    # then the client will send that JWT to the chat app which will have the same keys.
    connection_id = get_connection_id(event)

    body = get_body(event, ['token'])
    session = authenticate_token(body['token'])

    table = dynamodb.Table(CONNECTIONS_TABLE)
    table.update_item(Key={"connectionId": connection_id}, AttributeUpdates={
        'username': {
            'Value': session.username
        },
        'eventId': {
            'Value': session.eventId
        },
        'registrantId': {
            'Value': session.registrantId
        }
    })
    return get_response(200, {
        'username': session.username
    })


def get_message_list(messages):
    return [{
        "username": x["username"],
        "content": x["content"],
        "registrantId": int(x["registrantId"]),
        REQUEST_ID_FIELD: x[REQUEST_ID_FIELD],
        "timestamp": int(x["timestamp"])}
        for x in messages
    ]


@handler
def send_message(event, context):
    """
    When a message is sent on the socket, forward it to all connections.
    """

    session = get_session(get_connection_id(event))
    body = get_body(event, ['content'])
    request_id = get_request_id(event)

    table = dynamodb.Table(MESSAGES_TABLE)
    timestamp = int(time.time())
    username = session.username
    content = body["content"]

    message = {
        "eventId": session.eventId,
        "timestamp": timestamp,
        "username": username,
        "registrantId": session.registrantId,
        "content": content,
        TTL_FIELD: get_expiration(),
        REQUEST_ID_FIELD: request_id
    }

    table.put_item(Item=message)

    return get_response(200, "Message sent to all connections.")


def _image_to_item(image):
    def get_value(value):
        if 'N' in value:
            return int(value['N'])
        elif 'B' in value:
            return True if value['B'].lower() in {'true', '1'} else False
        return value['S']

    return dict(
        (key, get_value(value)) for (key, value) in image.items()
    )


@handler
def process_message(event, context):
    logger.info("Processing message")

    event_messages = {}

    for record in event['Records']:
        try:
            message = _image_to_item(record['dynamodb']['NewImage'])
            event_messages.setdefault(message['eventId'], []).append(message)
        except KeyError:
            logger.warning("Skipping record because of key error %s", record)

    for eventId, messages in event_messages.items():
        send_to_all_connections(messages, eventId, sender="processMessage")

    return get_response(200, "Message processed")


def feed_in_message(event, context):
    logger.info("Message fed into chat stream")

    for record in event['Records']:
        message = json.loads(record['body'])
    
        table = dynamodb.Table(MESSAGES_TABLE)

        message[REQUEST_ID_FIELD] = None
        message[TTL_FIELD] = get_expiration()

        table.put_item(Item=message)

    return get_response(200, "Message feed into chat stream")

def send_to_all_connections(messages, eventId, sender):
    table = dynamodb.Table(CONNECTIONS_TABLE)
    response = table.scan(ProjectionExpression="connectionId",
                          FilterExpression=Attr('eventId').eq(eventId))
    items = response.get("Items", [])
    connections = [x["connectionId"] for x in items if "connectionId" in x]

    # Send the message data to all connections
    data = get_message_list(messages)
    response = get_response(200, {"messages": data}).to_websocket(sender=sender)

    for connectionId in connections:
        send_to_connection(connectionId, response)


@handler
def get_recent_messages(event, context):
    """
    Return the 10 most recent chat messages.
    """
    connection_id = get_connection_id(event)
    session = get_session(connection_id)

    # Get the 10 most recent chat messages
    table = dynamodb.Table(MESSAGES_TABLE)
    response = table.query(KeyConditionExpression="eventId = :eventId",
                           ExpressionAttributeValues={
                               ":eventId": session.eventId},
                           Limit=10, ScanIndexForward=False)
    messages = get_message_list(reversed(response.get("Items", [])))

    return get_response(200, {"messages": messages})


@handler
def default_message(event, context):
    """
    Send back error when unrecognized WebSocket action is received.
    """
    logger.info("Unrecognized WebSocket action received.")
    return get_response(400, "Unrecognized WebSocket action.")


@handler
def ping(event, context):
    logger.info("Ping")
    return get_response(200, {
        'message': 'PONG'
    })
