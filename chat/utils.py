import boto3
import json
import logging
from functools import wraps
from botocore.exceptions import ClientError
from datetime import datetime, timedelta

from .models import Response
from .constants import REQUEST_ID_FIELD, API_GATEWAY_ENDPOINT

logger = logging.getLogger(__name__)

gatewayapi = boto3.client("apigatewaymanagementapi",
                          endpoint_url=API_GATEWAY_ENDPOINT)

def send_to_connection(connection_id, data):
    try:
        gatewayapi.post_to_connection(ConnectionId=connection_id,
                                         Data=json.dumps(data).encode('utf-8'))
    except ClientError as e:
        logger.warning("Failed to send response to websocket connection %s, %s", connection_id, e)

def get_response(status_code, body) -> Response:
    return Response(status_code=status_code, body=body)

class StatusError(Exception):
    def __init__(self, status_code, body):
        self.status_code = status_code
        self.body = body

        super().__init__(body)

def handler(func):
    @wraps(func)
    def decorated(event, context):
        try:
            resp = func(event, context)
        except StatusError as e:
            resp = get_response(e.status_code, e.body)
        except Exception as e:
            logger.error("Internal error %s", e)
            resp = get_response(500, 'Internal server error')

        connection_id = get_connection_id(event)

        if connection_id is not None:
            # Try to match the action name if this was triggered directly
            sender = get_action_name(event) or func.__name__
            request_id = get_request_id(event)

            send_to_connection(connection_id, resp.to_websocket(request_id=request_id, sender=sender))

        return resp.to_lambda()

    return decorated

def get_connection_id(event):
    return event.get("requestContext", {}).get("connectionId")


def get_body(event, attributes=[]):
    try:
        body = json.loads(event.get("body", ""))
    except:
        logger.debug("event body could not be JSON decoded.")
        raise StatusError(400, "event body could not be JSON decoded")

    for attribute in attributes:
        if attribute not in body:
            logger.debug("Failed: '%s' not in message dict.", attribute)
            raise StatusError(400, "'{}' not in message dict"
                              .format(attribute))
    return body


def get_action_name(event):
    try:
        return get_body(event).get('action')
    except StatusError:
        return None


def get_request_id(event):
    try:
        return get_body(event).get(REQUEST_ID_FIELD)
    except StatusError:
        return None


def add_request_id(body, request_id):
    body[REQUEST_ID_FIELD] = request_id
    return body

def get_expiration():
    # 30 days in the future
    ttl = datetime.now() + timedelta(days=30)
    return int(ttl.timestamp())
