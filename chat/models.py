import json
import typing
from dataclasses import dataclass

from .constants import REQUEST_ID_FIELD


@dataclass
class Session:
    username: str
    eventId: str
    viewer_id: int


@dataclass
class Response:
    status_code: int
    body: typing.Union[dict, str]

    def to_websocket(self, sender, request_id=None):
        resp = dict(**self.get_body(), status_code=self.status_code, sender=sender)

        if request_id:
            resp[REQUEST_ID_FIELD] = request_id

        return resp

    def to_lambda(self):
        return {
            'statusCode': self.status_code,
            'body': json.dumps(self.get_body())
        }

    def get_body(self):
        if isinstance(self.body, str):
            return {'message': self.body}
        return self.body
